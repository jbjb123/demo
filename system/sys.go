package main

import (
	"fmt"
	"time"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
)

/*
func main() {
	/*
		p, _ := cpu.Percent(time.Second, false)
		fmt.Printf("CPU:%.1f%%", p[0])


	//加入for遍历动态显示
	for {
		p, _ := cpu.Percent(time.Second, false)
		fmt.Printf("CPU:%.1f%%", p[0])
		time.Sleep(time.Second)
	}

}
*/

func main() {
	cpuInfos, err := cpu.Info()
	if err != nil {
		fmt.Printf("get cpu info failed, err:%v", err)

	}
	for _, ci := range cpuInfos {
		fmt.Println(ci)
		getCpuLoad()
		getHostInfo()
		getDiskInfo()
	}
	// CPU使用率
	for {
		percent, _ := cpu.Percent(time.Second, false)
		//fmt.Printf("cpu percent:%v\n", percent)
		fmt.Printf("\rcpu percent:%.1f%%", percent)
	}

}

func getCpuLoad() {
	info, _ := load.Avg()
	fmt.Printf("%.1f%%\n", info)
}

func getHostInfo() {
	hInfo, _ := host.Info()
	fmt.Printf("host info:%v uptime:%v boottime:%v\n", hInfo, hInfo.Uptime, hInfo.BootTime)
}

func getDiskInfo() {
	parts, err := disk.Partitions(true)
	if err != nil {
		fmt.Printf("get Partitions failed, err:%v\n", err)
		return
	}
	for _, part := range parts {
		fmt.Printf("part:%v\n", part.String())
		diskInfo, _ := disk.Usage(part.Mountpoint)
		fmt.Printf("disk info:used:%v free:%v\n", diskInfo.UsedPercent, diskInfo.Free)
	}

	ioStat, _ := disk.IOCounters()
	for k, v := range ioStat {
		fmt.Printf("%v:%v\n", k, v)
	}
}
