package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// 定义一个全局对象db
var db *sql.DB

// 定义一个初始化数据库的函数
func initDB() (err error) {
	dsn := "mimi:123qwe@tcp(192.168.8.111:43306)/word?charset=utf8mb4&parseTime=True"
	// 不会校验账号密码是否正确
	// 注意！！！这里不要使用:=，我们是给全局变量赋值，然后在main函数中使用全局变量db
	db, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	}
	// 尝试与数据库建立连接（校验dsn是否正确）
	err = db.Ping()
	if err != nil {
		return err
	}
	return nil
}

type User struct {
	user_id    int
	umeta_id   int
	umeta_key  string
	meta_value string
}

func query() {
	s := "select * from qwusermeta"
	r, err := db.Query(s)
	var u User
	defer r.Close()

	if err != nil {
		fmt.Printf("err: %v\n", err)
	} else {

		/*
			for r.Next() {

				r.Scan(&u.user_id, &u.umeta_id, &u.umeta_key, &u.meta_value)

					fmt.Printf("u: %v\n", u)
					//a := fmt.Printf("u: %v\n", u)
		*/

		for r.Next() {
			r.Scan(&u.user_id, &u.umeta_id, &u.umeta_key, &u.meta_value)
			fmt.Printf("u: %v\n", u)

			for i := 0; 5 < u; i++ {
				i++
				if i > 10 {
					fmt.Printf("u: %v\n", u)
					break
				}
			}

		}

	}

}

func main() {
	err := initDB() // 调用输出化数据库的函数
	if err != nil {
		fmt.Printf("初始化失败！,err:%v\n", err)
		return
	} else {
		fmt.Printf("初始化成功")
	}
	query()
}
