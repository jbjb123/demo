package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/shirou/gopsutil/mem"
)

/*
func main() {

	//p, _ := cpu.Percent(time.Second, false)
	//fmt.Printf("CPU:%.1f%%", p[0])

	data := [][]string{}
	p_Percent, _ := cpu.Percent(time.Second, false)
	p_count, _ := cpu.Counts(true)

	table := tabwriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"项目", "数量", "百分比"})

	data = append(data, []string{"CPU", fmt.Sprintf("%d核", p_count),
		fmt.Sprintf("%.1f%%", p_Percent[0])})
	for _, v := range data {
		table.Append(v)
	}
	table.Render()

}
*/

func main() {

	data := [][]string{}
	v, _ := mem.VirtualMemory()
	fmt.Printf("全部:%dG", v.Total/1024/1024/1024)
	fmt.Printf("\r已经使用内存:%.1f%%", v.UsedPercent)
	fmt.Printf("\r闲置可用内存:%dG-", v.Available/1024/1024/1024)
	//需要转型
	num, _ := strconv.ParseFloat(fmt.Sprintf("\r已使用百分比:%.2f", v.UsedPercent), 64)
	fmt.Println(num)

	data = append(data, []string{"CPU", fmt.Sprintf("%d核", v.Total/1024/1024/1024)})
	data = append(data, []string{"MEM", fmt.Sprintf("%dG", v.Available/1024/1024/1024)})
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"项目", "数量"})

	for _, v := range data {
		table.Append(v)

	}
	table.Render()
}

//https://github.com/olekukonko/tablewriter
//https://www.cnblogs.com/binHome/p/12238070.html
//https://github.com/shirou/gopsutil
//https://www.jtthink.com/course/play/3666
