package main

import (
	"fmt"
	"net/http"
	"runtime"

	"github.com/gin-gonic/gin"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	"github.com/shirou/gopsutil/process"
	//"gitlab.com/tingshuo/go-diskstate/diskstate"
)

type Disk struct {
	Total int `json:"总共Total"`
	Free  int `json:"剩余Free"`
	Used  int `json:"使用Used"`
}

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("templates/**/*")

	//获取主机名
	nInfo, _ := host.Info()

	//获取当前负载情况
	infoc, _ := load.Avg()

	//
	v, _ := mem.VirtualMemory()

	//cpu详情
	b, _ := cpu.Times(true)

	//进程详情
	pi, _ := process.Pids()

	//进程统计
	bc := len(pi) //获取元素的个数

	//磁盘1
	parts, _ := disk.Partitions(true)
	for _, part := range parts {
		//fmt.Printf("part:%v\n", part.String())
		diskInfo, _ := disk.Usage(part.Mountpoint)

		//磁盘2
		//state := diskstate.DiskUsage("/")

		//网卡
		netinfo, _ := net.IOCounters(true)

		for index, s := range netinfo {
			fmt.Printf("网卡使用情况: %v:%v\n send:%v\n recv:%v\n", index, s, s.BytesSent, s.BytesRecv)

			router.GET("/", func(c *gin.Context) {
				c.String(http.StatusOK, "www", gin.H{})
			})

			router.GET("/sys/index", func(c *gin.Context) {
				c.HTML(http.StatusOK, "sys/index.tmpl", gin.H{
					"title1":  nInfo.Hostname,
					"title2":  infoc.String(),
					"title3":  v.UsedPercent,
					"title4":  v.Available / 1024 / 1024,
					"title5":  v.Total / 1024 / 1024 / 1024,
					"title6":  runtime.NumCPU(), //cpu物理cpu统计
					"title7":  b,
					"title8":  bc, //进程统计
					"title9":  pi, //进程详情
					"title10": diskInfo,
					"title11": netinfo,
					//"title11": state.All / diskstate.MB, state.Free / diskstate.MB, state.Available / diskstate.MB, state.Used / diskstate.MB, 100 * state.Used / state.All,
					"title20": Disk{
						Used:  int(diskInfo.Used / 1024 / 1024 / 1024),
						Free:  int(diskInfo.Free / 1024 / 1024 / 1024),
						Total: int(diskInfo.Total / 1024 / 1024 / 1024),
					},
				})
			})
			router.Run(":8080")
		}

	}

}
