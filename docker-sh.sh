#!/bin/bash

podman stop $(podman ps -q)
podman rm $(podman ps -aq)

rm -rf /etc/ceph /var/lib/ceph /var/log/ceph
mkdir -p /etc/ceph /var/lib/ceph /var/log/ceph


 podman run -d --name ceph-mon -p 6789:6789 --network ceph-network --ip 172.20.0.10 -e CLUSTER=ceph -e WEIGHT=1.0 -e MON_IP=172.20.0.10 -e MON_NAME=ceph-mon -e CEPH_PUBLIC_NETWORK=172.20.0.0/16 -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -v /var/log/ceph/:/var/log/ceph/ ceph/daemon:latest-bis-nautilus mon

podman exec ceph-mon ceph auth get client.bootstrap-osd -o /var/lib/ceph/bootstrap-osd/ceph.keyring

cat <<END>  /etc/ceph/ceph.conf
[global]
fsid = 0f231994-2c2a-4fd4-8275-b832801bb648
mon initial members = ceph-mon
mon host = 172.20.0.10
public network = 172.20.0.0/16
cluster network = 172.20.0.0/16
osd journal size = 100
[mon]
mon addr = 192.168.8.78:6789
[osd.3]
172.20.0.11
[osd.0]
172.20.0.12
[osd.2]
172.20.0.13

osd max object name len = 256
osd max object namespace len = 64

END

mkdir /var/lib/ceph/osd/1 -p
mkdir /var/lib/ceph/osd/2 -p
mkdir /var/lib/ceph/osd/3 -p
podman run -d --privileged=true --name ceph-osd-1 --network ceph-network --ip 172.20.0.11 -e CLUSTER=ceph -e WEIGHT=1.0 -e MON_NAME=ceph-mon -e MON_IP=172.20.0.10 -e OSD_TYPE=directory -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -v /var/lib/ceph/osd/1:/var/lib/ceph/osd -v /etc/localtime:/etc/localtime:ro ceph/daemon:latest-bis-nautilus osd

podman run -d --privileged=true --name ceph-osd-2 --network ceph-network --ip 172.20.0.12 -e CLUSTER=ceph -e WEIGHT=1.0 -e MON_NAME=ceph-mon -e MON_IP=172.20.0.10 -e OSD_TYPE=directory -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -v /var/lib/ceph/osd/2:/var/lib/ceph/osd -v /etc/localtime:/etc/localtime:ro ceph/daemon:latest-bis-nautilus osd

podman run -d --privileged=true --name ceph-osd-3 --network ceph-network --ip 172.20.0.13 -e CLUSTER=ceph -e WEIGHT=1.0 -e MON_NAME=ceph-mon -e MON_IP=172.20.0.10 -e OSD_TYPE=directory -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ -v /var/lib/ceph/osd/3:/var/lib/ceph/osd -v /etc/localtime:/etc/localtime:ro ceph/daemon:latest-bis-nautilus osd

podman run -d --privileged=true --name ceph-mgr --network ceph-network --ip 172.20.0.14 -e CLUSTER=ceph -p 7000:7000 --pid=container:ceph-mon -v /etc/ceph:/etc/ceph -v /var/lib/ceph/:/var/lib/ceph/ ceph/daemon:latest-bis-nautilus mgr
podman exec ceph-mgr ceph mgr module enable dashboard
podman exec ceph-mon ceph auth get client.bootstrap-rgw -o /var/lib/ceph/bootstrap-rgw/ceph.keyring

podman run -d --privileged=true --name ceph-rgw --network ceph-network --ip 172.20.0.15 -e CLUSTER=ceph -e RGW_NAME=ceph-rgw -p 7480:7480 -v /var/lib/ceph/:/var/lib/ceph/ -v /etc/ceph:/etc/ceph -v /etc/localtime:/etc/localtime:ro ceph/daemon:latest-bis-nautilus rgw

podman run -d --privileged=true --name ceph-mds --network ceph-network --ip 172.20.0.22 -e CLUSTER=ceph -e MDS_NAME=ceph-mds -v /var/lib/ceph/:/var/lib/ceph/ -v /etc/ceph:/etc/ceph -v /etc/localtime:/etc/localtime:ro ceph/daemon:latest-bis-nautilus mds


podman exec ceph-mon ceph -s
podman exec ceph-mon  ceph osd pool create fs_kube_data 32 32
podman exec ceph-mon ceph osd pool create fs_kube_metadata 32 32
podman exec ceph-mon  ceph fs new cephfs fs_kube_metadata fs_kube_data
